
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel='icon' href='{{asset("images/logo.png")}}' type='image/x-icon'/ >
  <title>Sign In</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  @include('layouts.base_css')

</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><img width="20%" src='{{asset("images/logo.png")}}'></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="input-group mb-3">
          <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
          id="email" name="email" placeholder="Email" value="{{ old('email') }}" maxlength="100" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-sign-in-alt"></span>
            </div>
          </div>
        </div>
        <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
        @if ($errors->has('email'))
            <span class="text-danger">
              <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
        </div>
        <div class="input-group mb-3">
            <input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" name="password" placeholder="Password" maxlength="15" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
        </div>

        <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
        @if ($errors->has('password'))
          <span class="text-danger">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
        @endif
        </div>

        <p class="mb-1">
          <a href="{{url('register')}}">Register</a>
        </p>

        <div class="row">
          <!-- /.col -->
          <div class="col-12">
            <button type="submit" class="quilt-button btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <br>
      <center><p style="font-size:12px;">Copyright © 2020 {{config('app.name')}} | All rights reserved</P></center>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

@include('layouts.base_js')

</body>
</html>
