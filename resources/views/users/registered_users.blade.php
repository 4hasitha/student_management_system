@extends('layouts.base')

@section('content')
<!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Users</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Users</a></li>
            <li class="breadcrumb-item"><a href="#">Registered</a></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <!-- form start -->

              <!-- /.row -->
                <form action="{{url('users')}}" enctype="multipart/form-data" method="get">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-6">&nbsp;</div>
                      <div class="col-6">
                          Search:
                          <input type="text" name="search_key" value="{{$searchKey}}" placeholder="Name / Email" class="form-control">
                          <button style="margin-top:10px;margin-bottom:10px;" class="quilt-button btn btn-success float-right">Search</button>
                      </div>
                    </div>
                  </div>
                </form>
                <div class="card-body">
                  <!-- fetch alerts -->
                  <div class="form-group">
                    @include('components.alerts')
                  </div>
                    <div class="row overflow-auto">
                      <div class="col-12">
                        <table class="table table-striped">
                          <tr>
                              <th>Name</th>
                              <th>Role</th>
                              <th>Email</th>
                              <th>#</th>
                          </tr>

                          @foreach($users as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{getRole($user->role)}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    @if($user->role == "1")
                                        <a href="{{url('courses/enrolled-courses')}}?user_id={{$user->id}}" class="btn btn-primary">Enrolled Courses</a>
                                    @else

                                    @endif
                                </td>
                            </tr>
                          @endforeach
                        </table>
                      </div>
                    </div>
                </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
    </div>
  </section>
@endsection

@section('additional-scripts')
<script type="text/javascript">
  $( document ).ready(function() {

  });

</script>
@endsection
