@extends('layouts.base')

@section('content')
<!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">My Profile</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">My Profile</a></li>
            <li class="breadcrumb-item"><a href="#">Update</a></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <!-- form start -->

              <form id="addMasterItem" method="POST" autocomplete="off" action="{{ route('users.update') }}/{{Auth::id()}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="put" />
                <div class="card-body">
                  <!-- fetch alerts -->
                  <div class="form-group">
                    @include('components.alerts')
                  </div>
                    <div class="row">
                      <div class="col-6">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          <label for="name">Name *</label>
                          <input id="name" type="text" class="form-control" placeholder="Eg.: Mike" name="name" value="{{$user->name}}" maxlength="255">
                          @error('name')
                              <div class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>
                      </div>

                      <div class="col-6">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                          <label for="email">Email *</label>
                          <input id="email" type="email" class="form-control" placeholder="Eg.: mike@gmail.com" name="email" value="{{$user->email}}" maxlength="255">
                          @error('email')
                              <div class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>
                      </div>

                      <div class="col-12" style="font-size:12px;">Please leave blank if you will not want to change the password.</div>

                      <div class="col-6">
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                          <label for="password">Password</label>
                          <input id="password" type="password" class="form-control" placeholder="Eg.: 5345gdg$" name="password" value="" maxlength="10">
                          @error('password')
                              <div class="help-block text-danger">{{ $message }}</div>
                          @enderror
                        </div>
                      </div>

                      <div class="col-6">
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                          <label for="password">Confirm Password</label>
                          <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Eg.: 5345gdg$" maxlength="10">
                        </div>
                      </div>
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="quilt-button btn btn-warning float-right">Update</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
        </div>
    </div>
  </section>
@endsection

@section('additional-scripts')
<script type="text/javascript">
  $( document ).ready(function() {

  });

</script>
@endsection
