@extends('layouts.base')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">User Enrolled Courses - {{$user->name}}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Users</a></li>
                        <li class="breadcrumb-item"><a href="#">Enrolled Courses</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(Auth::user()->role == "0")
                        <a href="{{url('users')}}" class="btn btn-success">Users</a>
                        <br><br>
                    @endif
                    <div class="card card-primary">
                        <!-- form start -->

                        <!-- /.row -->
                        <div class="card-body">
                            <!-- fetch alerts -->
                            <div class="form-group">
                                @include('components.alerts')
                            </div>
                            <div class="row overflow-auto">
                                <div class="col-12">
                                    <table class="table table-striped">
                                        <tr>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                        </tr>

                                        @foreach($courses as $course)
                                            <tr>
                                                <td>{{$course->course->code}}</td>
                                                <td>{{$course->course->name}}</td>
                                                <td>{{$course->course->description}}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
@endsection

@section('additional-scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

        });

    </script>
@endsection
