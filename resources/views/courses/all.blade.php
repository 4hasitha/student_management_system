@extends('layouts.base')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Users</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Users</a></li>
                        @if(Auth::user()->role == "0")
                            <li class="breadcrumb-item"><a href="#">Registered</a></li>
                        @elseif(Auth::user()->role == "1")
                            <li class="breadcrumb-item"><a href="#">Enroll Courses</a></li>
                        @endif
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary">
                        <!-- form start -->

                        <!-- /.row -->
                        <form action="{{url('courses')}}" enctype="multipart/form-data" method="get">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">&nbsp;</div>
                                    <div class="col-6">
                                        Search:
                                        <input type="text" name="search_key" value="{{$searchKey}}" placeholder="Code / Name / Description" class="form-control">
                                        <button style="margin-top:10px;margin-bottom:10px;" class="quilt-button btn btn-success float-right">Search</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="card-body">
                            <!-- fetch alerts -->
                            <div class="form-group">
                                @include('components.alerts')
                            </div>
                            <div class="row overflow-auto">
                                <div class="col-12">
                                    {{$courses->appends(request()->input())->links()}}
                                    <table class="table table-striped">
                                        <tr>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Status</th>
                                        </tr>

                                        @foreach($courses as $course)
                                            <tr>
                                                <td>{{$course->code}}</td>
                                                <td>{{$course->name}}</td>
                                                <td>{{$course->description}}</td>
                                                <td>
                                                    @if(Auth::user()->role == "0")
                                                        @if($course->status == "1")
                                                            <a href="{{url('courses/change-status')}}?course_id={{$course->id}}" class="btn btn-danger">Deactivate</a>
                                                        @else
                                                            <a href="{{url('courses/change-status')}}?course_id={{$course->id}}" class="btn btn-success">Activate</a>
                                                        @endif
                                                    @elseif(Auth::user()->role == "1")
                                                        @if(checkEnrolled(Auth::id(), $course->id))
                                                            Already Enrolled
                                                        @else
                                                            <a href="{{url('courses/enroll')}}?course_id={{$course->id}}" class="btn btn-primary">Enroll Me</a>
                                                        @endif
                                                    @endif
                                                    </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
@endsection

@section('additional-scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

        });

    </script>
@endsection
