@extends('layouts.base')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">New Course</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Courses</a></li>
                        <li class="breadcrumb-item"><a href="#">New</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary">
                        <!-- form start -->

                        <form id="addMasterItem" method="POST" autocomplete="off" action="{{url('courses/create')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <!-- fetch alerts -->
                                <div class="form-group">
                                    @include('components.alerts')
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                                            <label for="code">Code *</label>
                                            <input id="code" type="text" class="form-control" placeholder="Eg.: 4353" name="code" value="{{old('code')}}" maxlength="255" required>
                                            @error('code')
                                            <div class="help-block text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                            <label for="name">Name *</label>
                                            <input id="name" type="text" class="form-control" placeholder="Eg.: Sinhala" name="name" value="{{old('name')}}" maxlength="255" required>
                                            @error('name')
                                            <div class="help-block text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                            <label for="description">Description *</label>
                                            <textarea id="description" type="text" class="form-control" placeholder="Eg.: Mike" name="description" value="{{old('description')}}" maxlength="500" required></textarea>
                                            @error('description')
                                            <div class="help-block text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="quilt-button btn btn-primary float-right">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
@endsection

@section('additional-scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

        });

    </script>
@endsection
