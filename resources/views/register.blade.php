<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center" style="background-color: white">
    <h1>Registration</h1>
    <p>Course Enroll System</p>
</div>

<div class="container">
    <div class="row">
        <div class="col-12">

                <form id="addMasterItem" method="POST" autocomplete="off" action="{{url('register')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                        <!-- fetch alerts -->
                        <div class="form-group">
                            @include('components.alerts')
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name">Name *</label>
                                    <input id="name" type="text" class="form-control" placeholder="Eg.: Mike" name="name" value="{{old('name')}}" maxlength="255">
                                    @error('name')
                                    <div class="help-block text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email">Email *</label>
                                    <input id="email" type="email" class="form-control" placeholder="Eg.: mike@gmail.com" name="email" value="{{old('email')}}" maxlength="255">
                                    @error('email')
                                    <div class="help-block text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-6"></div>

                            <div class="col-12"><hr></div>

                            <div class="col-6">
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password">Password *</label>
                                    <input id="password" type="password" class="form-control" placeholder="Eg.: 5345gdg$" name="password" value="" maxlength="10">
                                    @error('password')
                                    <div class="help-block text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password">Confirm Password *</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Eg.: 5345gdg$" maxlength="10">
                                </div>
                            </div>
                        </div>

                    <div class="card-footer">
                        <div class="row">
                            <button type="submit" class="quilt-button btn btn-primary float-right">Submit</button>
                            <a href="{{url('login')}}" class="btn btn-success">Login</a>
                        </div>
                    </div>
                </form>
            <!-- /.card -->
        </div>
    </div>
</div>

</body>
</html>
