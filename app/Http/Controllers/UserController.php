<?php

namespace App\Http\Controllers;

use App\Mail\StudentRegistration;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Mail;

class UserController extends Controller
{
//  public function __construct()
//  {
//    $this->middleware('auth');
//  }

  public function index(Request $request)
  {
    $searchKey = $request->search_key;
    $users = User::getUsers($searchKey);
    return view('users.registered_users',compact('users','searchKey'));
  }

  public function create()
  {
    return view('users.new_user');
  }

  public function store(Request $request)
  {
    User::validateRequest($request);
    $user = new User;
    $user->fill($request->all());
    $user->password = bcrypt($request->password);
    $user->save();

    return back()->with('status','New user is saved successfully.');
  }

  public function show(User $user)
  {
    if($user->id==Auth::id()){
      return view('users.profile',compact('user'));
    }else {
      echo "You have no permissions to view others' accounts.";
    }
  }

  public function update(User $user,Request $request)
  {
    if($user->id==Auth::id()){
      User::validateRequestEdit($request);
      $user = User::find($user->id);
      $user->name = $request->name;
      if($request->email!=$user->email){
        if(!User::where('email',$request->email)->first())
          $user->email = $request->email;
        else
          return back()->with('warning',"You entered email is already existing with the system.");
      }
      if($request->password!=""){
        if(strlen($request->password)>=6)
          $user->password = bcrypt($request->password);
        else
          return back()->with('warning',"Your password is should be more than 6 characters.");
      }
      $user->save();
    }else {
      return back()->with('warning',"You have no permissions to edit others' accounts.");
    }

    return back()->with('status','Your profile is updated successfully.');
  }

    public function register(User $user)
    {
        return view("register");
    }

    public function registerGuest(Request $request)
    {
        $user = new User;
        $user->fill($request->all());
        $user->password = bcrypt($request->password);
        $user->save();

        Mail::to($request->email)->send(new StudentRegistration());

        Auth::login($user);

        return redirect("/home");
    }
}
