<?php

namespace App\Http\Controllers;

use App\Course;
use App\CourseUser;
use App\User;
use Illuminate\Http\Request;
use Auth;

class CourseController extends Controller
{
    public function index(Request $request)
    {
        $searchKey = $request->search_key;
        if(Auth::user()->role == "1"){
            $courses = Course::where("status", "1")
            ->where(
                function($query) use ($searchKey) {
                    return $query
                        ->where("code", "%$searchKey%")
                        ->orWhere("description", "like", "%$searchKey%")
                        ->orWhere("name", "like", "%$searchKey%");
                })->paginate(50);
        }else{
            $courses = Course::where("code", "%$searchKey%")
                ->orWhere("description", "like", "%$searchKey%")
                ->orWhere("name", "like", "%$searchKey%")->paginate(50);
        }

        return view('courses.all',compact('courses','searchKey'));
    }

    public function create()
    {
        return view('courses.new');
    }

    public function store(Request $request)
    {
        Course::validateRequest($request);
        $course = new Course;
        $course->fill($request->all());
        $course->save();

        return back()->with('status','New course is saved successfully.');
    }

    public function changeStatus(Request $request)
    {
        $course = Course::find($request->course_id);
        if(isset($course)){
            if($course->status == "1"){
                $course->status = "0";
            }else{
                $course->status = "1";
            }
        }else{
            $course->save();
            return back()->with("danger", "There are no course from you selected.");
        }
        $course->save();

        return back()->with("status", "Status is changed for particular course successfully.");
    }

    public function coursesEnroll(Request $request)
    {
        $courseStudent = CourseUser::where("course_id", $request->course_id)
            ->where("user_id", Auth::id())->first();

        if(isset($courseStudent)){
            return back()->with("danger", "You have already enrolled to this course.");
        }else{
            $courseUser = new CourseUser();
            $courseUser->user_id = Auth::id();
            $courseUser->course_id = $request->course_id;
            $courseUser->save();
            return back()->with("status", "You have successfully enrolled to this course");
        }
    }

    public function enrolledCourses(Request $request)
    {
        if(Auth::user()->role == "0"){
            $courses = CourseUser::with("user", "course")->where("user_id", $request->user_id)->get();
            $user = User::where("id", $request->user_id)->first();
        }else{
            $courses = CourseUser::with("user", "course")->where("user_id", Auth::id())->get();
            $user = User::where("id", Auth::id())->first();
        }

        return view("courses.enrolled_courses", compact("courses", "user"));
    }
}
