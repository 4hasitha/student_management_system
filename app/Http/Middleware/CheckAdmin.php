<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Auth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user =  User::where("id", Auth::id())->first();
        if($user->role == "0"){
            return $next($request);
        }else{
            return "Permission Denied";
        }
    }
}
