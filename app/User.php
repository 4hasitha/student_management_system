<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
         'name','email','role','password',
     ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function validateRequest($request){
      return $request->validate([
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users',
        'role' => 'required|regex:/^[0-1]+$/u|string|max:1',
        'password' => 'required|string|min:6|max:10|confirmed',
      ]);
    }

    public static function validateRequestEdit($request){
      return $request->validate([
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255',
      ]);
    }

    public static function getUsers($searchKey){
      return User::where('name','like',"%$searchKey%")
      ->orWhere('email','like',"%$searchKey%")->get();
    }
}
