<?php

use App\CourseUser;

function getRole($role){
    if($role=="0"){
      return "Admin";
    }elseif($role=="1"){
      return "Student";
    }
  }

function checkEnrolled($userId, $courseId){
    $courseStudent = CourseUser::where("course_id", $courseId)
        ->where("user_id", $userId)->first();

    if(isset($courseStudent)){
        return true;
    }else{
        return false;
    }
}
