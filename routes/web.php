<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/register', function () {
    return view('auth.login');
});

Route::get('/home', 'DashboardController@index')->name('home');
Route::get('/users/create', 'UserController@create')->middleware("check_admin");
Route::post('/users/create', 'UserController@store')->middleware("check_admin");
Route::get('/users', 'UserController@index')->middleware("check_admin");
Route::get('/users/{user?}', 'UserController@show')->name('users.show')->middleware("auth");
Route::put('/users/{user?}', 'UserController@update')->name('users.update')->middleware("auth");

Route::get('/courses', 'CourseController@index')->middleware("auth");
Route::get('/courses/create', 'CourseController@create')->middleware("check_admin");
Route::post('/courses/create', 'CourseController@store')->middleware("check_admin");
Route::get('/courses/change-status', 'CourseController@changeStatus')->middleware("check_admin");
Route::get('/courses/enroll', 'CourseController@coursesEnroll')->middleware("check_student");
Route::get('/courses/enrolled-courses', 'CourseController@enrolledCourses')->middleware("auth");

Route::get('/register', 'UserController@register');
Route::post('/register', 'UserController@registerGuest');
