<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        'name' => 'Hasitha Wijayasuriya',
        'email' => '4hasitha@gmail.com',
        'role' => '0',
        'password' => bcrypt('123456'),
      ]);

        DB::table('users')->insert([
            'name' => 'Kamal Silva',
            'email' => 'kamal@gmail.com',
            'role' => '1',
            'password' => bcrypt('123456'),
        ]);

//        courses
        DB::table('courses')->insert([
            'code' => '3435',
            'name' => 'Sinhala',
            'description' => '',
        ]);

        DB::table('courses')->insert([
            'code' => '9789',
            'name' => 'IT',
            'description' => '',
        ]);

        DB::table('courses')->insert([
            'code' => '8796',
            'name' => 'History',
            'description' => '',
        ]);

        DB::table('courses')->insert([
            'code' => '9807',
            'name' => 'Music',
            'description' => '',
        ]);

        DB::table('courses')->insert([
            'code' => '5780',
            'name' => 'Maths',
            'description' => '1',
        ]);
    }
}
